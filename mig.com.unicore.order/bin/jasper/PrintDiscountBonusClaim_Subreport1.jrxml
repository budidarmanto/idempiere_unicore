<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="PrintDiscountBonusClaim_Subreport1" language="groovy" pageWidth="555" pageHeight="802" columnWidth="555" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="9ec8f2ea-5b60-4692-a6f2-8e811040e9d5">
	<property name="ireport.zoom" value="1.2100000000000055"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="UNS_BonusClaim_ID" class="java.lang.Integer"/>
	<queryString>
		<![CDATA[SELECT mp.Name AS product, ds.ValidFrom AS ValidFrom, ds.ValidTo AS ValidTo, ds.DocumentNo AS DiscountSchema, COALESCE(ds.ReferenceNo, '') AS RefNo, inv.DateInvoiced AS DateInvoice, bpl.BPartnerName AS Customer, inv.DocumentNo AS Invoice, line.QtyInvoiced AS Qty, line.PriceList AS Price, inv.AddDiscount AS InvoiceDisc, inv.AddDiscountAmt AS InvoiceDiscAmt, discpercclaim(line.C_OrderLine_ID, 'Y') AS PercClaim, calcdiscclaim(line.C_OrderLine_ID, 'Y') AS DiscClaim, discpercclaim(line.C_OrderLine_ID, 'N') AS PercReg, calcdiscclaim(line.C_OrderLine_ID, 'N') AS DiscReg, ROUND(tax.Rate,2) AS Rate

FROM UNS_BonusClaim bc

INNER JOIN UNS_BonusClaimLine bcl ON bcl.UNS_BonusClaim_ID = bc.UNS_BonusClaim_ID
INNER JOIN UNS_DiscountTrx trx ON trx.UNS_BonusClaimLine_ID = bcl.UNS_BonusClaimLine_ID
INNER JOIN M_Product mp ON mp.M_Product_ID = bcl.M_Product_ID
INNER JOIN M_DiscountSchemaBreak dsb ON dsb.M_DiscountSchemaBreak_ID = bc.M_DiscountSchemaBreak_ID
INNER JOIN M_DiscountSchema ds ON ds.M_DiscountSchema_ID = dsb.M_DiscountSchema_ID
INNER JOIN C_InvoiceLine line ON line.C_InvoiceLine_ID = trx.C_InvoiceLine_ID
INNER JOIN C_Invoice inv ON inv.C_Invoice_ID = line.C_Invoice_ID
INNER JOIN C_BPartner bp ON bp.C_BPartner_ID = bc.C_BPartner_ID
INNER JOIN C_BPartner cust ON cust.C_BPartner_ID = inv.C_BPartner_ID
INNER JOIN C_BPartner_Location bpl ON bpl.C_BPartner_Location_ID = inv.C_BPartner_Location_ID
INNER JOIN C_Tax tax ON tax.C_Tax_ID = bcl.C_Tax_ID

WHERE bc.UNS_BonusClaim_ID = $P{UNS_BonusClaim_ID}]]>
	</queryString>
	<field name="product" class="java.lang.String"/>
	<field name="validfrom" class="java.sql.Timestamp"/>
	<field name="validto" class="java.sql.Timestamp"/>
	<field name="discountschema" class="java.lang.String"/>
	<field name="refno" class="java.lang.String"/>
	<field name="dateinvoice" class="java.sql.Timestamp"/>
	<field name="customer" class="java.lang.String"/>
	<field name="invoice" class="java.lang.String"/>
	<field name="qty" class="java.math.BigDecimal"/>
	<field name="price" class="java.math.BigDecimal"/>
	<field name="invoicedisc" class="java.math.BigDecimal"/>
	<field name="invoicediscamt" class="java.math.BigDecimal"/>
	<field name="percclaim" class="java.math.BigDecimal"/>
	<field name="discclaim" class="java.math.BigDecimal"/>
	<field name="percreg" class="java.math.BigDecimal"/>
	<field name="discreg" class="java.math.BigDecimal"/>
	<field name="rate" class="java.math.BigDecimal"/>
	<variable name="nomor" class="java.lang.Integer" calculation="Count">
		<variableExpression><![CDATA[$V{nomor} == null ? 1 : $V{nomor} + 1]]></variableExpression>
	</variable>
	<variable name="t_qty" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{qty}]]></variableExpression>
	</variable>
	<variable name="t_price" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{price}.multiply($F{qty})]]></variableExpression>
	</variable>
	<variable name="t_discclaim" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{discclaim}]]></variableExpression>
	</variable>
	<variable name="t_discreg" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{discreg}]]></variableExpression>
	</variable>
	<variable name="t_invoicedisc" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{invoicedisc}]]></variableExpression>
	</variable>
	<variable name="ppn" class="java.math.BigDecimal">
		<variableExpression><![CDATA[($V{t_discclaim}.divide(100)).multiply($F{rate})]]></variableExpression>
	</variable>
	<variable name="t_netto" class="java.math.BigDecimal">
		<variableExpression><![CDATA[$V{t_discclaim}.add($V{ppn})]]></variableExpression>
	</variable>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="69" splitType="Stretch">
			<textField>
				<reportElement uuid="e5400c64-b113-4d02-b317-4c01175bfc3d" x="0" y="0" width="555" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="13" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["REKAP DISCOUNT PROMO " + $F{product}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="180a2a5e-5b9a-4141-8022-a97cf54bb9c3" x="0" y="20" width="555" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="13" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[new SimpleDateFormat("dd-MM").format($F{validfrom}) + " s/d " + new SimpleDateFormat("dd-MM").format($F{validto}) + " (" + new SimpleDateFormat("yyyy").format($F{validfrom}) + ")"]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="f0870c39-0c73-4fdc-97ba-7b855375a87c" x="0" y="40" width="555" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="13" isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA["" + $F{discountschema} + $F{refno} == null ? "" : " - " + $F{refno}]]></textFieldExpression>
			</textField>
		</band>
	</title>
	<columnHeader>
		<band height="82" splitType="Stretch">
			<staticText>
				<reportElement uuid="3c271e1a-a2bb-4728-9cc0-35d6f6a1a54a" x="0" y="0" width="21" height="61"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[No]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="3a970e98-03fe-462f-bbd1-def7d6b5e6a0" x="21" y="0" width="51" height="61"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Tanggal]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="c0b03a05-fe3d-41b1-96f4-822d7626fcaf" x="72" y="0" width="147" height="61"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Nama Toko]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="2edcee8f-8dc7-40ad-8ac2-2759f33b615f" x="219" y="0" width="36" height="61"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Faktur]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f4bfd47b-0d3e-42ad-94d0-ae7805a4a9fe" x="291" y="0" width="59" height="40"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[R'bp]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="90c2a193-f9e6-4cf7-bd39-3614466a1e16" x="350" y="20" width="68" height="20"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Invoice]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="efb4f877-d179-4f30-b39a-5918982357f7" x="418" y="20" width="68" height="20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle" markup="none"/>
				<text><![CDATA[Beban]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="e0065bc9-b391-47cf-a7a5-0810bb5fbde4" x="350" y="0" width="204" height="20"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Diskon Promo]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="b8eb3e1b-0d95-4f5b-a160-02137be824e3" x="255" y="0" width="36" height="61"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Jumlah]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="4d9801c0-b2a0-4f05-946e-ac466ada8b49" x="486" y="20" width="68" height="20"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Beban]]></text>
			</staticText>
			<textField pattern="@#,##0">
				<reportElement uuid="d91b327e-607c-4f68-9c8d-808533d057dc" x="291" y="40" width="59" height="21"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle" markup="none"/>
				<textFieldExpression><![CDATA[$F{price}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="1a59d9c0-74d8-41ff-b043-e4f5f1c7ce51" x="350" y="40" width="23" height="21"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[%]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="56d11811-18c5-4bc7-a9e2-56806fdb825e" x="373" y="40" width="45" height="21"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Rp]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="46630dc1-676a-4de2-8bfe-2f0493387ada" x="441" y="40" width="45" height="21"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Rp]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="3f3f90aa-d4aa-4fad-9769-b080758c0df8" x="418" y="40" width="23" height="21"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[%]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="fdcd9022-b88b-44a6-ae21-7c0b5a4890d7" x="509" y="40" width="45" height="21"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[Rp]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="d051d860-b7dd-4efc-b5cd-5763864840b1" x="486" y="40" width="23" height="21"/>
				<box>
					<topPen lineWidth="1.0"/>
					<leftPen lineWidth="1.0"/>
					<bottomPen lineWidth="1.0"/>
					<rightPen lineWidth="1.0"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[%]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f043d781-d0a7-4386-9bec-0e3c61765805" x="0" y="61" width="21" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="609f9df8-a883-4827-acab-052fbdb4f4d0" x="21" y="61" width="51" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="89a11c54-92f9-4b99-8d7e-f47eebf83b8f" x="72" y="61" width="147" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f35de0bd-d217-4e22-b14d-71ca54d6a03e" x="219" y="61" width="36" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="73b3a637-7cf3-4458-b885-4232472f2363" x="255" y="61" width="36" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="1ce6c366-d401-49cd-b247-7cb3781d5cf7" x="291" y="61" width="59" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="489c1c8c-b6ba-4ed8-a21b-fc8cc2610ea4" x="350" y="61" width="23" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="e42ce336-d4eb-4df5-b4de-95a1ffca9a9d" x="373" y="61" width="45" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="073a80df-3460-403a-99c7-8187753dd886" x="418" y="61" width="23" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="e358d0a5-30b8-4efc-a84f-6127b1fec2da" x="441" y="61" width="45" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="c992e2b3-c3d4-4d56-8d8c-51ac994a9195" x="486" y="61" width="23" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="5610951e-b18b-49e3-a488-1860c3652513" x="509" y="61" width="45" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="20" splitType="Stretch">
			<textField pattern="dd/MM/yyyy">
				<reportElement uuid="328a4133-4cf7-406f-abad-3de7924a4aeb" x="21" y="0" width="51" height="20"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{dateinvoice}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="286dde6d-fa1f-419b-bcac-9fc0a2710541" x="72" y="0" width="147" height="20"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{customer}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="8d97ca5b-caea-4202-abde-dc5aee029e86" x="219" y="0" width="36" height="20"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{invoice}]]></textFieldExpression>
			</textField>
			<textField pattern="###0">
				<reportElement uuid="1b67df7d-0c72-4374-84bc-bc3179cac727" x="255" y="0" width="36" height="20"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{qty}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement uuid="51f053e4-7e3a-4bb9-a646-6880535a3333" x="0" y="0" width="21" height="20"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{nomor}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="8c53026e-cd27-4b77-914a-545b53b18659" x="291" y="0" width="59" height="20"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{price}.multiply($F{qty})]]></textFieldExpression>
			</textField>
			<textField pattern="###0.00">
				<reportElement uuid="7850a603-60c9-4e88-90d7-13d0cfd738f5" x="350" y="0" width="23" height="20"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{invoicedisc}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="e2a9a131-1b8a-4e16-b6ca-c23ae5ca3717" x="373" y="0" width="45" height="20"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{invoicediscamt}]]></textFieldExpression>
			</textField>
			<textField pattern="###0.00">
				<reportElement uuid="29385462-b5b1-4b31-8113-0ec58b23d1bc" x="418" y="0" width="23" height="20"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{percclaim}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="5f647aca-8ec5-43e7-8c9e-97128a9dd156" x="441" y="0" width="45" height="20"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{discclaim}]]></textFieldExpression>
			</textField>
			<textField pattern="###0.00">
				<reportElement uuid="6688bdf5-eff1-4640-813c-1764c7030ac4" x="486" y="0" width="23" height="20"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{percreg}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="30a41546-74d7-443a-93d1-4ef5529542d8" x="509" y="0" width="45" height="20"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{discreg}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="84" splitType="Stretch">
			<staticText>
				<reportElement uuid="f35de0bd-d217-4e22-b14d-71ca54d6a03e" x="219" y="0" width="36" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="609f9df8-a883-4827-acab-052fbdb4f4d0" x="21" y="0" width="51" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="1ce6c366-d401-49cd-b247-7cb3781d5cf7" x="291" y="0" width="59" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f043d781-d0a7-4386-9bec-0e3c61765805" x="0" y="0" width="21" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="e358d0a5-30b8-4efc-a84f-6127b1fec2da" x="441" y="0" width="45" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="489c1c8c-b6ba-4ed8-a21b-fc8cc2610ea4" x="350" y="0" width="23" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="c992e2b3-c3d4-4d56-8d8c-51ac994a9195" x="486" y="0" width="23" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="5610951e-b18b-49e3-a488-1860c3652513" x="509" y="0" width="45" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="89a11c54-92f9-4b99-8d7e-f47eebf83b8f" x="72" y="0" width="147" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="73b3a637-7cf3-4458-b885-4232472f2363" x="255" y="0" width="36" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="e42ce336-d4eb-4df5-b4de-95a1ffca9a9d" x="373" y="0" width="45" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="073a80df-3460-403a-99c7-8187753dd886" x="418" y="0" width="23" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f043d781-d0a7-4386-9bec-0e3c61765805" x="0" y="21" width="21" height="63"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f043d781-d0a7-4386-9bec-0e3c61765805" x="21" y="21" width="234" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Total Bruto]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f043d781-d0a7-4386-9bec-0e3c61765805" x="21" y="42" width="234" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[PPN]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="f043d781-d0a7-4386-9bec-0e3c61765805" x="21" y="63" width="234" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<text><![CDATA[Total Netto]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="73b3a637-7cf3-4458-b885-4232472f2363" x="255" y="42" width="36" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="73b3a637-7cf3-4458-b885-4232472f2363" x="255" y="63" width="36" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<textField pattern="###0">
				<reportElement uuid="7849075f-4cd2-4c2c-a63b-5ed3782577b0" x="255" y="21" width="36" height="21"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" markup="none"/>
				<textFieldExpression><![CDATA[$V{t_qty}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="73b3a637-7cf3-4458-b885-4232472f2363" x="291" y="42" width="59" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="73b3a637-7cf3-4458-b885-4232472f2363" x="291" y="63" width="59" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<textField pattern="#,##0">
				<reportElement uuid="c6d0beab-1b3e-4718-9f56-e8b3b7918198" x="291" y="21" width="59" height="21"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" markup="none"/>
				<textFieldExpression><![CDATA[$V{t_price}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="73b3a637-7cf3-4458-b885-4232472f2363" x="350" y="42" width="68" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="73b3a637-7cf3-4458-b885-4232472f2363" x="350" y="63" width="68" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<textField pattern="#,##0">
				<reportElement uuid="e217f1f4-58ec-46f1-b099-f6fd30b6a4b2" x="350" y="21" width="68" height="21"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" markup="none"/>
				<textFieldExpression><![CDATA[$V{t_invoicedisc}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement uuid="46063d2b-c75b-4080-9e6d-ea837f5ccfc5" x="418" y="42" width="68" height="21"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" markup="none"/>
				<textFieldExpression><![CDATA[$V{ppn}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="4962a167-bbc4-4700-9ec3-bb7647e7c848" x="418" y="21" width="68" height="21"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" markup="none"/>
				<textFieldExpression><![CDATA[$V{t_discclaim}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement uuid="73b3a637-7cf3-4458-b885-4232472f2363" x="486" y="63" width="68" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<staticText>
				<reportElement uuid="73b3a637-7cf3-4458-b885-4232472f2363" x="486" y="42" width="68" height="21"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[]]></text>
			</staticText>
			<textField pattern="#,##0">
				<reportElement uuid="7f3060d0-97d8-4d53-892e-eff558ba7384" x="486" y="21" width="68" height="21"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" markup="none"/>
				<textFieldExpression><![CDATA[$V{t_discreg}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0">
				<reportElement uuid="46063d2b-c75b-4080-9e6d-ea837f5ccfc5" x="418" y="63" width="68" height="21"/>
				<box leftPadding="1" rightPadding="1">
					<pen lineWidth="0.5"/>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle" markup="none"/>
				<textFieldExpression><![CDATA[$V{t_netto}]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
