/**
 * 
 */
package org.uns.matica.form.listener;

import org.matica.form.listener.MAT_I_Listener;

/**
 * @author Haryadi
 *
 */
public interface UNS_I_Listener extends MAT_I_Listener {

	public Object getFormFrame();
}
